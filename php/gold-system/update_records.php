<?php
function updateRecord($con, $user, $recordColumn, $value, $greatestOrLeast) {
	$updateGoldRecordStatement = $con->prepare("INSERT INTO records (user, ".$recordColumn.") VALUES (:user, :value) ON DUPLICATE KEY UPDATE ".$recordColumn." = ".$greatestOrLeast."(".$recordColumn.", VALUES(".$recordColumn."))");
	$updateGoldRecordStatement->execute(array(
		':user' => $user,
		':value' => $value
	));
}

function updateRecordTotal($con, $user, $recordColumn, $value) {
	$updateGoldRecordStatement = $con->prepare("INSERT INTO records (user, ".$recordColumn.") VALUES (:user, :value) ON DUPLICATE KEY UPDATE ".$recordColumn." = ".$recordColumn." + ".$value);
	$updateGoldRecordStatement->execute(array(
		':user' => $user,
		':value' => $value
	));
}

function updateRecords($user, $gold, $newTotal, $command) {
	global $MYSQL_CONFIG;
	try {
        $con = new PDO('mysql:host=' . $MYSQL_CONFIG['host'] . ';dbname=' . $MYSQL_CONFIG['db'],
        	$MYSQL_CONFIG['user'], $MYSQL_CONFIG['pass']);
	} catch(PDOException $e) {
        echo $e->getMessage();
        die();
	}

	// Check for new gold record.
	updateRecord($con, $user, 'gold_record', $newTotal, 'GREATEST');	
	
	// Check for new biggest win / loss.
	if ($command != 'give' && $command != 'toorich') {
		if ($gold > 0) {
			updateRecord($con, $user, 'biggest_win', abs($gold), 'GREATEST');	
			// Update total winnings.
			updateRecordTotal($con, $user, 'total_winnings', $gold);
		} else {
			updateRecord($con, $user, 'biggest_loss', abs($gold), 'GREATEST');			
			// Update total losses.
			updateRecordTotal($con, $user, 'total_losses', abs($gold));
		}
	} else {
		// Check for total giving / receiving.
		if ($gold > 0) {
			updateRecordTotal($con, $user, 'total_receiving', $gold);
		} else {
			updateRecordTotal($con, $user, 'total_giving', abs($gold));
		}
		
	}

	// Check realm first achievements.
}

?>