<?php
/*
	Returns an random user that have been active in the last 10 minutes.
	It excludes the calling users, the channel and the bot.
*/
function getRandomActiveUser($user) {
	global $CURRENCY_DB;
	$db = new SQLite3($CURRENCY_DB);
	$db->busyTimeout(5000);
	$gold = 0;
	$activeTime = date('Y-m-d H:i:s', time() - 10 * 60);
	$randuser = null;

	$getActiveUsersStatement = $db->prepare('SELECT Name FROM CurrencyUser WHERE LastSeen >= :activeTime AND Name != :user AND Name NOT LIKE :bot AND Name != :channel');
	$getActiveUsersStatement->bindParam(':activeTime', $activeTime);
	$getActiveUsersStatement->bindParam(':user', strtolower($user));
	$getActiveUsersStatement->bindValue(':bot', '%dwarvenoverlord%');
	$getActiveUsersStatement->bindValue(':channel', 'dwarftopia');
	$activeUsersResult = $getActiveUsersStatement->execute();

	$activeUsers = array();
	$count = 0;
	while ($res = $activeUsersResult->fetchArray(SQLITE3_ASSOC)) {
		$activeUsers[$count] = $res['Name'];
		$count++;
	}

	if (count($activeUsers) > 0) {
		$randuser = $activeUsers[mt_rand(0, $count - 1)];	
	}	

	//echo '<pre>';
	//print_r($activeUsers);
	//echo '</pre>';

	$db->close();
	unset($db);

	return $randuser;
}
?>