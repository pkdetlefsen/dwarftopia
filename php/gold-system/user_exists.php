<?php
/*
	Returns whether the given user exists or not.
*/
function userExists($user) {
	global $CURRENCY_DB;
	$db = new SQLite3($CURRENCY_DB);
	$db->busyTimeout(5000);
	$gold = 0;

	$findUserStatement = $db->prepare('SELECT * FROM CurrencyUser WHERE Name = :user');
	$findUserStatement->bindParam(':user', strtolower($user));
	$findResult = $findUserStatement->execute();

	$userExists = true;
	if ($findResult->fetchArray(SQLITE3_ASSOC) == false) {
		// User doesn't exist yet.
		$userExists = false;	
	} 
	$db->close();
	unset($db);

	return $userExists;
}
?>