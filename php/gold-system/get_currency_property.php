<?php
/*
	Fetches the users given property from the currency DB.
*/
function getCurrencyProperty($user, $propertyName) {
	global $CURRENCY_DB;
	$db = new SQLite3($CURRENCY_DB);
	$db->busyTimeout(5000);
	$property = '';

	$getPropertyStatement = $db->prepare('SELECT ' . $propertyName . ' FROM CurrencyUser WHERE Name = :user');
	$getPropertyStatement->bindParam(':user', strtolower($user));
	$propertyResult = $getPropertyStatement->execute();

	$resultArray = $propertyResult->fetchArray(SQLITE3_ASSOC);
	if ($resultArray == false) {
		// User doesn't exist yet.
		createUser($db, $user, 0);		

		$propertyResult = $getPropertyStatement->execute();
		$resultArray = $propertyResult->fetchArray(SQLITE3_ASSOC);
	} 
	
	$property = $resultArray[$propertyName];
	
	$db->close();
	unset($db);

	return $property;
}
?>