<?php 
/*
	Create the user with the given amount of gold.
*/
function createUser($db, $user, $amount) {
	$addUserStatement = $db->prepare("INSERT INTO CurrencyUser (Name, Rank, Points, Hours, Raids, LastSeen) 
			VALUES (:user, :rank, :points, :hours, :raids, :lastseen)");
	$addUserStatement->bindParam(':user', strtolower($user));  
	$addUserStatement->bindValue(':rank', 'Rank 1');		
	$addUserStatement->bindParam(':points', round($amount));		
	$addUserStatement->bindValue(':hours', '00:00:00');		
	$addUserStatement->bindValue(':raids', 0);		
	$addUserStatement->bindValue(':lastseen', date('Y-m-d H:i:s'));		

	$addUserStatement->execute();
}
?>