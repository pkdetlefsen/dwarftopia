<?php
/*
	Updates the given users gold with the given amount. 
	Amount can be be both positive and negative.
*/
function addGold($user, $amount, $command) {
	global $CURRENCY_DB;	
	$db = new SQLite3($CURRENCY_DB);
	$db->busyTimeout(5000);

	$checkGoldStatement = $db->prepare('SELECT Points FROM CurrencyUser WHERE Name = :user');
	$checkGoldStatement->bindParam(':user', strtolower($user));
	$checkResult = $checkGoldStatement->execute();

	$resultArray = $checkResult->fetchArray(SQLITE3_ASSOC);

	if ($resultArray == false) {
		// User doesn't exist yet.		
		createUser($db, $user, max(0, round($amount)));
	} else {
		// User exists. Update gold.
		$newAmount = max(0, $resultArray['Points'] + round($amount));
		$addGoldStatement = $db->prepare("UPDATE CurrencyUser SET Points = :newAmount, LastSeen = :lastSeen WHERE Name = :user");
		$addGoldStatement->bindParam(':user', strtolower($user));
		$addGoldStatement->bindParam(':newAmount', $newAmount);
		$addGoldStatement->bindValue(':lastSeen', date('Y-m-d H:i:s'));

		$results = $addGoldStatement->execute();

		updateRecords($user, $amount, $newAmount, $command);		 	
	}
	$db->close();
	unset($db);
}
?>