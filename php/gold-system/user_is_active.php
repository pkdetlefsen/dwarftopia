<?php
/*
	Returns whether the given user is active not.
*/
function userIsActive($user) {
	global $CURRENCY_DB;
	$db = new SQLite3($CURRENCY_DB);
	$db->busyTimeout(5000);
	$isActive = true;
	$activeTime = date('Y-m-d H:i:s', time() - 10 * 60);

	$getLastSeenStatement = $db->prepare('SELECT LastSeen FROM CurrencyUser WHERE Name = :user');
	$getLastSeenStatement->bindParam(':user', strtolower($user));
	$lastSeenResult = $getLastSeenStatement->execute();

	$res = $lastSeenResult->fetchArray(SQLITE3_ASSOC);
	if ($res == false) {
		// User doesn't exist so not active.
		$isActive = false;	
	} else {
		$isActive = ($res['LastSeen'] >= $activeTime) ? true : false;
	}

	$db->close();
	unset($db);

	return $isActive;
}
?>