<?php
/*
	Fetches the users position on the leaderboard.
*/
function getPosition($user) {
	global $CURRENCY_DB;
	$db = new SQLite3($CURRENCY_DB);
	$db->busyTimeout(5000);
	$position = 0;

	$getPositionStatement = $db->prepare(
		'SELECT 
		       (SELECT COUNT(*)
		        FROM CurrencyUser AS t2
		        WHERE t2.Points >= t1.Points AND
		        Name != "dwarftopia" AND Name NOT LIKE "%dwarvenoverlord%") AS Position
		FROM CurrencyUser AS t1
		WHERE Name = :user
		ORDER BY Points DESC;');
	$getPositionStatement->bindParam(':user', strtolower($user));
	$positionResult = $getPositionStatement->execute();

	$resultArray = $positionResult->fetchArray(SQLITE3_ASSOC);
	if ($resultArray == false) {
		// User doesn't exist yet.
		createUser($db, $user, 0);	
		// Get new users position.
		$positionResult = $getPositionStatement->execute();
		$resultArray = $positionResult->fetchArray(SQLITE3_ASSOC);
	} 
		
	$position = $resultArray['Position'];
	
	$db->close();
	unset($db);

	return $position;
}
?>