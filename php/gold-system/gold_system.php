<?php
/*
	The gold system contains all functions involving changes in gold.
*/

require_once '../utility/config.php'; // MySQL config etc.
require_once 'create_user.php'; // Needed in addGold and getGold.
require_once 'add_gold.php';
require_once 'user_exists.php';
require_once 'get_random_active_user.php';
require_once 'get_currency_property.php';
require_once 'get_position.php';
require_once 'get_records.php';
require_once 'update_records.php';
require_once 'user_is_active.php';
?>