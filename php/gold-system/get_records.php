<?php
/*
	Fetches data from the records table.	
*/
function getRecords($user, $column) {	
	global $MYSQL_CONFIG;
	$goldRecord = 0;
	try {
        $con = new PDO('mysql:host=' . $MYSQL_CONFIG['host'] . ';dbname=' . $MYSQL_CONFIG['db'],
        	$MYSQL_CONFIG['user'], $MYSQL_CONFIG['pass']);
	} catch(PDOException $e) {
        echo $e->getMessage();
        die();
	}

	$getRecordStatement = $con->prepare("SELECT " . $column . " FROM records WHERE user = :user");
	$getRecordStatement->execute(array(
		':user' => $user		
	));

	$results = $getRecordStatement->fetchAll(PDO::FETCH_ASSOC);

	if ($results != false && count($results) > 0) {
		//var_dump($results);
		$row = $results[0];
		$goldRecord = $row[$column];
	}

	return $goldRecord;
}

?>