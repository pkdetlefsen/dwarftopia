<?php                
/*      

Toorich v1.0

Usage: 
    !toorich [gold]

*/
require_once '../gold-system/gold_system.php';

$user = $_GET['sender'];
$messageArray = explode(' ', urldecode($_GET['message']));
$amount = 0;
$points = getCurrencyProperty($user, 'Points');
$giveLimit = round($points / 10);
$response = $user . ' ';
$event = '';
$randuser = getRandomActiveUser($user);

if (count($messageArray) >= 2) {
    $amount = round((float) $messageArray[1]);
}

if ($amount <= 0) {
    $response .= 'Usage: !toorich [gold] (min: 1)';
} else if ($amount > $giveLimit) {
    $response .= 'You can only give away 10% of your total gold. That\'s ' . number_format($giveLimit) . ' gold.';
} else if ($amount > 2000000000) {
    $response .= 'The maximum giving amount is 2,000,000,000.';
} else if ($randuser == null) {
    $response .= 'There were no other active users to give gold to.';
} else {
    addGold($user, -$amount, 'toorich');
    addGold($randuser, $amount, 'toorich');  

    $newUserTotal = $points - $amount;
    $response .= 'You randomly gave ' . number_format($amount) . ' gold to ' . $randuser . '. DBstyle You now have ' . number_format($newUserTotal) . ' gold.' . PHP_EOL;
    $response .= $randuser . ' You randomly received ' . number_format($amount) . ' gold from ' . $user . '. DBstyle';
        
    $event = $user . ' randomly gave ' . number_format($amount) . ' gold to ' . $randuser . '.';
    // Save event in DB.
    $eventQuery = 'user='.$user.'&command=toorich&amount='.$amount.'&description='.urlencode($event);
    file_get_contents('http://localhost:8080/dwarftopia/php/utility/save_event.php?'.$eventQuery, true); 
}

echo $response;
?>