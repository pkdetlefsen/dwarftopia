<?php                
/*      
Beg v1.0

Usage: 
    !beg

Odds at 0 gold:
 - Some gold gained = 100%
 - You get robbed = 0%

Odds at 1 - 1000+ gold:
 - Some gold gained = 95% - 50%
 - You get robbed = 5% - 50%
*/
require_once '../gold-system/gold_system.php';

function calcNoGoldChance($gold) {
    if ($gold == 0) return 0;
    return min(0.3, 0.3 * $gold / 1000) + 0.1;
}

function calcSomeGoldChance($gold) {
    if ($gold == 0) return 1;
    return 0.95 - min(0.45, 0.45 * $gold / 1000);
}

function calcRobbedChance($gold) {
    if ($gold == 0) return 0;
    return min(0.45, 0.45 * $gold / 1000) + 0.05;
}

$user = $_GET['sender'];
$response = $user . ' ';
$event = '';
$gold = getCurrencyProperty($user, 'Points');
$amount = 0;

// Calculate outcome chances.
$someGoldChance = calcSomeGoldChance($gold);

// Decide actual outcome.
$random = lcg_value();
if ($random <= $someGoldChance) {
    $amount = round(lcg_value() * 24) + 1;
    $newTotal = $amount + $gold;
    addGold($user, $amount, 'beg');
    $response .= 'The people of Dwarftopia saw your need and gave you ' . $amount . ' gold. DendiFace'
        . ' You now have ' . number_format($newTotal) . ' gold.';
    $event .= $user .' gained ' . $amount . ' gold by begging.';
} else {
    $amount = round((lcg_value() * 0.15 + 0.05) * $gold);
    $newTotal = $gold - $amount;
    addGold($user, -$amount, 'beg');
    $response .= 'The people of Dwarftopia discovered your true wealth and stole ' 
        . number_format($amount) . ' gold from you. SwiftRage You now have ' 
        . number_format($newTotal) . ' gold.';
    $event .= $user .' lost ' . number_format($amount) . ' gold by begging.';
}

$eventQuery = 'user='.$user.'&command=beg&amount='.$amount.'&description='.urlencode($event);
file_get_contents('http://localhost:8080/dwarftopia/php/utility/save_event.php?'.$eventQuery, true); 

echo $response;
?>