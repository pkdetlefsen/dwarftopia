<?php                
/*      

Gold v1.0

Usage: 
    !gold
*/
require_once '../gold-system/gold_system.php';

$user = $_GET['sender'];
$gold = getCurrencyProperty($user, 'Points');
$rank = getCurrencyProperty($user, 'Rank');
$hours = getCurrencyProperty($user, 'Hours');
$position = getPosition($user);
$goldRecord = getRecords($user, 'gold_record');

// Format hours.
$hourCount = 0;
$daysArray = explode('.', $hours);
if (count($daysArray) == 2) {
    // There are a days numbers.
    $hourCount += $daysArray[0] * 24;
    $hoursArray = explode(':', $daysArray[1]);  
} else {
    $hoursArray = explode(':', $hours);
}
$hourCount += $hoursArray[0] + $hoursArray[1] / 60;

$response = $user . ' ' . $rank . ' Dwarf | Hours: ' . round($hourCount, 1) 
    . ' | Gold: ' . $gold . ' | Record: ' . $goldRecord . ' | You\'re #' . $position;

echo $response;
?>