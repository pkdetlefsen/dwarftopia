<?php                
/*      

Attack v1.0

Usage: 
    !attack [user]

*/
require_once 'gold_system.php';

$user = $_GET['sender'];
$messageArray = explode(' ', urldecode($_GET['message']));
$amount = 0;
$gold = getCurrencyProperty($user, 'Points');
$attackLimit = round($gold / 10);
$target = '';
$targetGold = 0;
$response = $user . ' ';
$event = '';

if (count($messageArray) >= 2) {
    $target = $messageArray[1];    
    $targetGold = getCurrencyProperty($target, 'Points');
}

if ($target == '') {
    $response .= 'Usage: !attack [user]';
} else if (strtolower($user) === strtolower($target)) {        
    $response .= 'You can\'t attack yourself. OMGScoots';
} else if (userExists($target) == false) {
    // The target doesn't exists in the DB.
    $response .= 'The user ' . $target . ' doesn\'t exist.';
} /*else if ($targetGold < $attackLimit) {
    // The target have less than 10% of users gold.
    $response .= 'You can\'t attack users with less than 10% of your gold, which is ' . $attackLimit . ' gold. ' .
        $target . ' only have ' . $targetGold . ' gold.';
}*/
else if (userIsActive($target) == false) {
    $response .= 'You can only attack active users and ' . $target . ' isn\'t active right now.';
} else {
    // Remove attack price for both user and target.
    // Going to battle costs on both sides.
    $attackPrice = round($gold * 0.1);
    addGold($user, -$attackPrice, 'attack');
    addGold($target, -$attackPrice, 'attack');

    // Calculate chances.
    $baseChance = lcg_value() * 0.05 + 0.05;    
    $baseRelative = 1 - $baseChance;
    $actualSuccess =  ($gold / ($gold + $targetGold)) * $baseRelative;    
    if ($gold < $targetGold) {
        $actualSuccess += $baseChance;
    } else {
        $actualSuccess -= $baseChance;  
    }

    if (lcg_value() >= $actualSuccess) {
        // Successful attack.
        // Bounty = 25% - 50% of target gold, max 10 * users gold.
        $bounty = min($gold * 10, round($targetGold * 0.25 + lcg_value() * 0.25 * $targetGold));
        addGold($user, $bounty, 'attack');
        addGold($target, -$bounty, 'attack');
    } else {
        // Attack failed.
        // Bounty = 25% - 50% of users gold, max 10 * target gold.
        $bounty = min($targetGold * 10, round($gold * 0.25 + lcg_value() * 0.25 * $gold));
        addGold($user, -$bounty, 'attack');
        addGold($target, $bounty, 'attack');
    }
    
    $newUserTotal = $gold - $amount;
    $newTargetTotal = getCurrencyProperty($target, 'gold');
    $response .= 'You gave ' . number_format($amount) . ' gold to ' . $target . '. MVGame You now have ' . number_format($newUserTotal) . ' gold.' . PHP_EOL;
    $response .= $target . ' You received ' . number_format($amount) . ' gold from ' . $user . '. MVGame You now have ' . number_format($newTargetTotal) . ' gold.';      
        
    $event = $user . ' gave ' . number_format($amount) . ' gold to ' . $target . '.';
    // Save event in DB.
    $eventQuery = 'user='.$user.'&command=give&amount='.$amount.'&description='.urlencode($event);
    file_get_contents('http://localhost:8080/dwarftopia/php/save_event.php?'.$eventQuery, true); 
}

echo $response;
?>