<?php
/*      

Start v1.0

Usage: 
    !start

*/
require_once '../gold-system/gold_system.php';
require_once '../utility/config.php';

global $MYSQL_CONFIG;        
        
try {
        $con = new PDO('mysql:host=' . $MYSQL_CONFIG['host'] . ';dbname=' . $MYSQL_CONFIG['db'],
                $MYSQL_CONFIG['user'], $MYSQL_CONFIG['pass']);
} catch(PDOException $e) {
        echo $e->getMessage();
        die();
}

$user = $_GET['sender'];
$message = urldecode($_GET['message']);
$response = $user . ' ';
$startingGold = 25;
$event = '';

// Check if the user already used the !start command
// to prevent getting extra gold.
$fetchEventsStatement = $con->prepare('SELECT user FROM start WHERE user = :user');
$fetchEventsStatement->execute(array(
	':user' => $user
));

$hasStarted = $fetchEventsStatement->fetchAll(PDO::FETCH_ASSOC);

if (count($hasStarted) >= 1) {
	$response .= 'Well met ' . $user . '! You already received your ' . $startingGold . ' starting gold. The goal is to become the richest dwarf in the kingdom. The game is played by typing commands in the chat. You can check your wealth with !gold and play the game using !beg, !coinflip and !sacrifice etc. On the stream you can see who\'s currently in the lead and what happened to your fellow dwarves. More info is in the description. Enjoy your stay here!';
} else {
	$response .= 'Well met ' . $user . ' and welcome to Dwarftopia! You just got ' . $startingGold . ' starting gold 4Head The goal is to become the richest dwarf in the kingdom. The game is played by typing commands in the chat. You can check your wealth with !gold and play the game using !beg, !coinflip and !sacrifice etc. On the stream you can see who\'s currently in the lead and what happened to your fellow dwarves. More info is in the stream description. Enjoy your stay here!';

	// Update the users gold amount.
	addGold($user, $startingGold, 'start');

	// Set the user as has started in the DB.
	$updateHasStartedStatement = $con->prepare('INSERT INTO start (user) VALUES (:user)');
	$updateHasStartedStatement->execute(array(
		':user' => $user,
	));

	$event = $user . ' got ' . $startingGold . ' gold using start.';
	// Save event in DB.
	$eventQuery = 'user='.$user.'&command=start&amount=10&description='.urlencode($event);
	file_get_contents('http://localhost:8080/dwarftopia/php/utility/save_event.php?'.$eventQuery, true); 
}

echo $response;
?>