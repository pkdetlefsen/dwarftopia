<?php                
/*      

Help v1.0

Usage: 
    !help
   
*/

$user = $_GET['sender'];

$response = $user . ' Here\'s how it works in Dwarftopia! The goal of this game is to become the richest dwarf in the kingdom. You play by typing commands in the chat. Type the !start command to begin and use !gold, !beg, !coinflip, !sacrifice and !dragonhunt etc. to play. On the stream you can see who\'s currently in the lead and what happened to your fellow dwarves. More info is in the stream description. Enjoy your stay here!';

echo $response;
?>

