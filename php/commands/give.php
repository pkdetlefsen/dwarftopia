<?php                
/*      

Give v1.0

Usage: 
    !give [user] [gold]

*/
require_once '../gold-system/gold_system.php';

$user = $_GET['sender'];
$messageArray = explode(' ', urldecode($_GET['message']));
$amount = 0;
$points = getCurrencyProperty($user, 'Points');
$giveLimit = round($points / 10);
$target = '';
$response = $user . ' ';
$event = '';

if (count($messageArray) >= 2) {
    $target = $messageArray[1];
    if (count($messageArray) >= 3) {        
        $amount = round((float) $messageArray[2]);
    }
}

if ($target == '' || $amount <= 0) {
    $response .= 'Usage: !give [user] [gold] (min: 1)';
} else if (strtolower($user) === strtolower($target)) {        
    $response .= 'You can\'t give yourself gold. OMGScoots';
} else if ($amount > $giveLimit) {
    $response .= 'You can only give away 10% of your total gold. That\'s ' . number_format($giveLimit) . ' gold.';
} else if ($amount > 2000000000) {
    $response .= 'The maximum giving amount is 2,000,000,000.';
} else if (userExists($target) == false) {
    // The target doesn't exists in the DB.
    $response .= 'The user ' . $target . ' doesn\'t exist.';
} else {
    addGold($user, -$amount, 'give');
    addGold($target, $amount, 'give');    
    $newUserTotal = $points - $amount;
    $newTargetTotal = getCurrencyProperty($target, 'Points');
    $response .= 'You gave ' . number_format($amount) . ' gold to ' . $target . '. MVGame You now have ' . number_format($newUserTotal) . ' gold.' . PHP_EOL;
    $response .= $target . ' You received ' . number_format($amount) . ' gold from ' . $user . '. MVGame You now have ' . number_format($newTargetTotal) . ' gold.';      
        
    $event = $user . ' gave ' . number_format($amount) . ' gold to ' . $target . '.';
    // Save event in DB.
    $eventQuery = 'user='.$user.'&command=give&amount='.$amount.'&description='.urlencode($event);
    file_get_contents('http://localhost:8080/dwarftopia/php/utility/save_event.php?'.$eventQuery, true); 
}

echo $response;
?>