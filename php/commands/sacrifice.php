<?php

/*	

Sacrifice v1.0

Usage: 
	!sacrifice [gold]

*/
require_once '../gold-system/gold_system.php';

function calculatePayout($sacrifice) {
	$scale = 0;

	$random = mt_rand() / mt_getrandmax();

	if($random >= 0.99) // [99; 100[
		$factor = 10;
	else if($random >= 0.97)  // [97; 99[
		$factor = $random / 0.97 * 7.5;
	else if($random >= 0.92)  // [92; 97[
		$factor = $random / 0.92 * 5;
	else if($random >= 0.85) // [85; 92[
		$factor = $random / 0.85 * 2.5;
	else if($random >= 0.62) // [60; 85[
		$factor = $random / 0.62;
	else if($random >= 0.05) // [5; 60]
		$factor = $random / 0.65 * 0.5;
	else // [0; 5[
		$factor = 0;

	return (int) ($sacrifice * $factor);
}

function floatSacrifice($amount) {
	$max = 10 * (float) $amount;
	$multiplier = lcg_value() * lcg_value() * lcg_value();
	$payout = round($multiplier * $max);
	if ($payout == $amount) {
		return floatSacrifice($amount);
	}
	return $payout;
}

$user = $_GET['sender'];
$responseArray = explode(' ', urldecode($_GET['message']));
$response = $user . ' ';
$event = '';
$sacrifice = 0;
$points = getCurrencyProperty($user, 'Points');

if (count($responseArray) >= 2) {
    $sacrifice = round((float) $responseArray[1]);
}

if ($sacrifice <= 0) {
	$response .= 'Usage: !sacrifice [gold] (min: 1)';
} else if ($points < $sacrifice) {
	$response .= 'You only have ' . number_format($points) . ' gold. :(';		
} else {
	$response .= 'You sacrificed '. number_format($sacrifice) . ' gold to the Dwarven Overlord ';
	addGold($user, -$sacrifice, 'sacrifice');
	
	$payout = floatSacrifice($sacrifice);
	$newTotal = $points - $sacrifice + $payout;
	if ($payout >= $sacrifice) {
		$response .= 'and got blessed with '. number_format($payout) . ' gold in return! PogChamp You now have ' . number_format($newTotal) . ' gold.';
		$amount = $payout - $sacrifice;
		$event = $user . ' gained ' . number_format($amount) . ' gold on a sacrifice.';
	} else {
		$response .= 'and got cursed with '. number_format($payout) . ' gold in return! riPepperonis You now have ' . 
			number_format($newTotal) . ' gold.';
		$amount = $sacrifice - $payout;
		$event = $user . ' lost ' . number_format($amount) . ' gold on a sacrifice.';
	}

	addGold($user, $payout, 'sacrifice');	
	
	// Save event in DB.
	$eventQuery = 'user='.$user.'&command=sacrifice&amount='.$amount.'&description='.urlencode($event);
	file_get_contents('http://localhost:8080/dwarftopia/php/utility/save_event.php?'.$eventQuery);
}

echo $response;
?>