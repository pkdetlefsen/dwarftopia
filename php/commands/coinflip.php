<?php                
/*      

Coinflip v1.1

Usage: 
    !coinflip [gold]

*/
require_once '../gold-system/gold_system.php';

$user = $_GET['sender'];
$messageArray = explode(' ', urldecode($_GET['message']));
$response = $user . ' ';
$event = '';
$gold = 0;

if (count($messageArray) >= 2) {
    $gold = round((float) $messageArray[1]);
}

$userTotal = getCurrencyProperty($user, 'Points');
$event = '';

if ($gold <= 0) {
    $response .= 'Usage: !coinflip [gold] (min: 1)';
} else if ($userTotal < $gold) {
    // User doesn't have enough points to bet.
    $response .= 'You only have ' . number_format($userTotal) . ' gold. :(';
} else {
    if (mt_rand(0, 1) == 1) {
        // Player wins.
        $gold = round($gold * 1.5);
        $newTotal = $userTotal + $gold;
        $response .= 'You won ' . number_format($gold) . ' gold on the coin flip! PogChamp You now have ' . number_format($newTotal) .' gold.';
        $event = $user . ' gained ' . number_format($gold) . ' gold on a coin flip.';
        
        addGold($user, $gold, 'coinflip');
    } else {
        // Player lose.
        $newTotal = $userTotal - $gold;
        $response .= 'You lost ' . number_format($gold) . ' gold on the coin flip! BibleThump You now have ' . number_format($newTotal) .' gold.';
        $event = $user . ' lost ' . number_format($gold) . ' gold on a coin flip.'; 
                 
        addGold($user, -$gold, 'coinflip');
    }
    
    // Save event in DB.
    $eventQuery = 'user='.$user.'&command='.urlencode('coin flip').'&amount='.-$gold.'&description='.urlencode($event);
    file_get_contents('http://localhost:8080/dwarftopia/php/utility/save_event.php?'.$eventQuery, true); 
}

echo $response;
?>