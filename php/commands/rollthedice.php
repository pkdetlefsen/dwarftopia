<?php
/*

Roll the dice v1

Usage: 
    !rollthedice [gold]
   
*/

/*
    Calculates winnings based on bet and dice eyes.

    Dice 2 - 7 = lose
    Dice 8 - 9 = x1
    Dice 10 - 11 = x2
    Dice 12 = x3
*/
function calculateWinnings($gold, $dice) {
    if ($dice < 8) {
        return -$gold;
    } else if ($dice < 10) {
        return $gold;
    } else if ($dice < 12) {
        return $gold * 2;
    } else {
        return $gold * 3;
    }
}

require_once '../gold-system/gold_system.php';

global $MYSQL_CONFIG;
try {
        $con = new PDO('mysql:host=' . $MYSQL_CONFIG['host'] . ';dbname=' . $MYSQL_CONFIG['db'],
                $MYSQL_CONFIG['user'], $MYSQL_CONFIG['pass']);
} catch(PDOException $e) {
        echo $e->getMessage();
        die();
}

$user = $_GET['sender'];
$messageArray = explode(' ', urldecode($_GET['message']));
$response = $user . ' ';
$event = '';
$gold = 0;

if (count($messageArray) >= 2) {
    $gold = round((float) $messageArray[1]);
}

$userTotal = getCurrencyProperty($user, 'Points');

if ($gold <= 0) {
    $response .= 'Usage: !rollthedice [gold] or !rtd [gold] (min: 1)';
} else if ($userTotal < $gold) {
    // User doesn't have enough points to bet.
    $response .= 'You only have ' . number_format($userTotal) . ' gold. :(';
} else {
    $dice  = mt_rand(2, 12);

    $winnings = calculateWinnings($gold, $dice);

    $newTotal = $userTotal + $winnings;
    if ($winnings < 0) {
        $response .= 'You rolled '.$dice.' and lost ' . number_format(abs($winnings)) . ' gold on roll the dice! WutFace You now have ' . number_format($newTotal) .' gold.';
        $event = $user . ' lost ' . number_format(abs($winnings)) . ' gold on roll the dice.';
    } else {
        $response .= 'You rolled '.$dice.' and won ' . number_format($winnings) . ' gold on roll the dice! PogChamp You now have ' . number_format($newTotal) .' gold.';
        $event = $user . ' gained ' . number_format($winnings) . ' gold on roll the dice.';
    }

    addGold($user, $winnings, 'rollthedice');

    // Save event in DB.
    $eventQuery = 'user='.$user.'&command='.urlencode('roll the dice').'&amount='.$winnings.'&description='.urlencode($event);
    file_get_contents('http://localhost:8080/dwarftopia/php/utility/save_event.php?'.$eventQuery, true);
}

echo $response;
?>