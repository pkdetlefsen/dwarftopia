<?php                
/*      

Records v1.0

Usage: 
    !records
*/
require_once '../gold-system/gold_system.php';

$user = $_GET['sender'];
$goldRecord = getRecords($user, 'gold_record');
$biggestWin = getRecords($user, 'biggest_win');
$biggestLoss = getRecords($user, 'biggest_loss');
$totalWinnings = getRecords($user, 'total_winnings');
$totalLosses = getRecords($user, 'total_losses');
$totalGiving = getRecords($user, 'total_giving');
$totalReceiving = getRecords($user, 'total_receiving');

$response = $user. ' Gold Record: %d | Biggest Win: %d | Biggest Loss: %d | Gold Won: %d | Gold Lost: %d | Gold Given: %d | Gold Received: %d';
echo sprintf($response, $goldRecord, $biggestWin, $biggestLoss, $totalWinnings, $totalLosses, $totalGiving, $totalReceiving);
?>