<?php
/*
	Fetch the latest records from the DB and output as table content.
*/
require_once '../utility/config.php';

global $MYSQL_CONFIG;        
        
try {
        $con = new PDO('mysql:host=' . $MYSQL_CONFIG['host'] . ';dbname=' . $MYSQL_CONFIG['db'],
                $MYSQL_CONFIG['user'], $MYSQL_CONFIG['pass']);
} catch(PDOException $e) {
        echo $e->getMessage();
        die();
}

echo '<th colspan="2"><h2>Hall of Fame</h2></th>';

$columns = array(
	'gold_record' => 'Most Gold',
	'biggest_win' => 'Biggest Win',
	'biggest_loss' => 'Biggest Loss',
	'total_giving' => 'Most Gold Given',
	'total_receiving' => 'Most Gold Received',
	'total_winnings' => 'Most Gold Won',
	'total_losses' => 'Most Gold Lost');

foreach ($columns as $key => $value) {
	$fetchRecordStatement = $con->prepare('SELECT * FROM records ORDER BY ' . $key . ' DESC LIMIT 1');
	$fetchRecordStatement->execute();

	$results = $fetchRecordStatement->fetchAll(PDO::FETCH_ASSOC);

	foreach ($results as $row) {
	        echo '<tr><td colspan="2" class="biggerFont gold shadow">'.$value.'</td></tr>';
	        echo '<tr><td class="leftAlign leftPadding biggerFont">'.$row['user'].'</td><td class="biggerFont">'.number_format($row[$key]).'</td></tr>';
	}
}
?>