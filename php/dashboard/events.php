<?php
/*
	Fetch the latest events from the DB and output as table content.
*/
require_once '../utility/config.php';

global $MYSQL_CONFIG;        
        
try {
        $con = new PDO('mysql:host=' . $MYSQL_CONFIG['host'] . ';dbname=' . $MYSQL_CONFIG['db'],
                $MYSQL_CONFIG['user'], $MYSQL_CONFIG['pass']);
} catch(PDOException $e) {
        echo $e->getMessage();
        die();
}

$fetchEventsStatement = $con->prepare('SELECT * FROM events ORDER BY id DESC LIMIT 10');
$fetchEventsStatement->execute();

$results = $fetchEventsStatement->fetchAll(PDO::FETCH_ASSOC);

echo '<th><h2>Latest Events</h2></th>';

foreach ($results as $row) {
        echo '<tr><td>'.$row['description'].'</td></tr>';
}
?>