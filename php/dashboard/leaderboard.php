<?php
/*
	
*/
require_once '../utility/config.php';

global $CURRENCY_DB;

$db = new SQLite3($CURRENCY_DB);
$db->busyTimeout(5000);
$topTenStatement = $db->prepare("SELECT * FROM CurrencyUser WHERE Name != 'dwarftopia' AND Name NOT LIKE '%dwarvenoverlord%' ORDER BY Points DESC LIMIT 15");

$results = $topTenStatement->execute();

echo '<th colspan="3"><h2>Leaderboard</h2></th>
	  <tr><td><h3>#</h3></td><td><h3>Name</h3></td><td class="rightAlign"><h3>Gold</h3></tr>';
$number = 1;
$class = '';
while ($row = $results->fetchArray()) {
	switch ($number) {
		case '1':
			$class = 'gold shadow biggerFont';
			break;
		case '2':
			$class = 'silver shadow biggerFont';
			break;
		case '3':
			$class = 'bronze shadow biggerFont';
			break;
		
		default:
			$class = '';
			break;
	}
	echo '<tr><td>'.$number.'</td><td class="'.$class.'">'.$row['Name'].'</td><td class="rightAlign">'.number_format($row['Points']).'</td></tr>';
    $number++;
}

$db->close();
unset($db);
?>