<?php

$USER_ACCOUNT = 'Peder';

$APP_DATA = 'C:\Users\\' . $USER_ACCOUNT . '\AppData\Roaming';
$ANKHBOT_DB_DIR = $APP_DATA . '\AnkhHeart\AnkhBotR2\Twitch\Databases\\';
$CURRENCY_DB = $ANKHBOT_DB_DIR . 'CurrencyDB.sqlite';


class CurrencyPointsDB extends SQLite3
{
    function __construct()
    {
		global $CURRENCY_DB;

        $this->open($CURRENCY_DB);
    }
}

copy($CURRENCY_DB, $CURRENCY_DB . '.backup');

$db = new CurrencyPointsDB();
$db->exec('UPDATE CurrencyUser SET Points = 0');

?>