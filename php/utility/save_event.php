<?php
/*
	Fetch the latest events from the DB and output as table content.
*/
require_once '../utility/config.php';

if (isset($_GET['user']) && isset($_GET['command']) && isset($_GET['amount']) && isset($_GET['description'])) {
        $user  = $_GET['user']; 
        $command  = $_GET['command'];
        $amount = $_GET['amount'];
        $description  = $_GET['description'];
        global $MYSQL_CONFIG;        
        
        try {
                $con = new PDO('mysql:host=' . $MYSQL_CONFIG['host'] . ';dbname=' . $MYSQL_CONFIG['db'],
                        $MYSQL_CONFIG['user'], $MYSQL_CONFIG['pass']);
        } catch(PDOException $e) {
                echo $e->getMessage();
                die();
        }
        
        $fetchEventsStatement = $con->prepare('INSERT INTO events (user, command, amount, description) VALUES(:user, :command, :amount, :description)');
        $fetchEventsStatement->execute(array(
                        ':user'         => $user,
                        ':command'      => $command,
                        ':amount'       => $amount,
                        ':description'  => $description,                       
        ));              
}
?>