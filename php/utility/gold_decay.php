<?php                
/*      

Gold decay

*/
require_once '../gold-system/gold_system.php';

global $CURRENCY_DB;
$db = new SQLite3($CURRENCY_DB);
$db->busyTimeout(5000);

$decayGoldStatement = $db->prepare("UPDATE CurrencyUser SET Points = CAST(Points * 0.99 AS int) WHERE LastSeen < datetime('now', '-24 hours', 'localtime')");
$decayGoldResult = $decayGoldStatement->execute();

$db->close();
unset($db);

if ($decayGoldResult == false) {
    echo 'failure';
} else {
    echo 'success';
}
?>