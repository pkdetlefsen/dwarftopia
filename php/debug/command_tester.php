<?php

function sacrifice($sacrifice) {
	$scale = 0;

	$random = mt_rand() / mt_getrandmax();

	if($random >= 0.99) // [99; 100[
		$factor = 10;
	else if($random >= 0.97)  // [97; 99[
		$factor = $random / 0.97 * 7.5;
	else if($random >= 0.92)  // [92; 97[
		$factor = $random / 0.92 * 5;
	else if($random >= 0.85) // [85; 92[
		$factor = $random / 0.85 * 2.5;
	else if($random >= 0.62) // [60; 85[
		$factor = $random / 0.62 * 1.25;
	else if($random >= 0.05) // [5; 60]
		$factor = $random / 0.65 * 0.5;
	else // [0; 5[
		$factor = 0;

	return (int) ($sacrifice * $factor) - $sacrifice;
}

function newSacrifice($amount) {	
	return mt_rand(0, mt_rand(0, mt_rand(0, $amount * 10))) - $amount;
}

function floatSacrifice($amount) {
	$max = 10 * (float) $amount;
	$multiplier = lcg_value() * lcg_value() * lcg_value();
	return round($multiplier * $max - $amount);
}

function coinflip($amount) {
	if (mt_rand(0,1) == 1) {
		return $amount * 1.5;
	} else {
		return -$amount;
	}
}

function dragonhunt($amount) {
	if (mt_rand(0, 100) <= 33) {
		return $amount * 2.75;
	} else {
		return -$amount;
	}
}

function calculateWinnings($gold, $dice) {
    if ($dice < 8) {
        return -$gold;
    } else if ($dice < 10) {
        return $gold;
    } else if ($dice < 12) {
        return $gold * 2;
    } else {
        return $gold * 3;
    }
}

function rollthedice($amount) {
	$dice  = mt_rand(2, 12);

    return calculateWinnings($amount, $dice);
}

function attack($userGold, $targetGold) {
	// Remove attack price for both user and target.
    // Going to battle costs on both sides.
    $attackPrice = round(min($userGold * 0.1, $targetGold * 0.1));     

    // Calculate chances.
    $baseChance = lcg_value() * 0.1 - 0.05;
    $actualSuccess = min(0.75, $userGold / ($userGold + $targetGold));
    $actualSuccess = max(0.25, $actualSuccess);
    $actualSuccess += $baseChance;


    $bounty = min($userGold, $targetGold) / $actualSuccess;	    
    /*
    if ($userGold > $targetGold) {
    	$bounty = min($targetGold * 10, round($targetGold * 0.25 + lcg_value() * 0.25 * $targetGold));	    
        
    } else {
    	$bounty = min($userGold * 10, round($userGold * 0.25 + lcg_value() * 0.25 * $userGold));	
    	
    }
    */

    if (lcg_value() <= $actualSuccess) {
        // Successful attack.
        // Bounty = 25% - 50% of target gold, max 10 * users gold.

        
        return $bounty - $attackPrice;
    } else {
        // Attack failed.
        // Bounty = 25% - 50% of users gold, max 10 * target gold.
        
        return -$bounty - $attackPrice;
    }
}

$iterations = 100000;
$bet = 1000;
$win = 0;
$lose = 0;

$total = 0;
for ($i = 0; $i < $iterations; $i++) {
	$payout = rollthedice(1000);
	if ($payout > $bet) {
		$win++;
	} else {
		$lose++;
	}
	$total += $payout;

	echo $payout . '<br>';
}

$averageOutcome = $total / $iterations;
$averageGain = $averageOutcome / $bet * 100;
$winChance = $win / $iterations;
echo 'Average outcome: ' . $averageOutcome;
echo '  Average gain: ' . $averageGain . '%';
echo '<br>';
echo 'Win chance: ' . $winChance;

?>