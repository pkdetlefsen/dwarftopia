<?php
	require_once '../gold-system/gold_system.php';

	$int = (int) $_GET['int'];
	$float = (float) $_GET['float'];
	$mult = $float * $float;
	$randmax = mt_getrandmax();

	echo 'int: ' . $int . '<br>';
	echo 'float: ' . round($float) . '<br>';
	echo 'mult: ' . $mult . '<br>';
	echo 'randmax: ' . $randmax . '<br>';

	$randuser = getRandomActiveUser('spiritboar');
	//var_dump($randuser);

	$hours = getCurrencyProperty('spiritboar', 'Hours');
	$hourCount = 0;
	$daysArray = explode('.', $hours);
	if (count($daysArray) == 2) {
		// There are a days numbers.
		$hourCount += $daysArray[0] * 24;
		$hoursArray = explode(':', $daysArray[1]);	
	} else {
		$hoursArray = explode(':', $hours);	
	}
	
	$hourCount += $hoursArray[0] + $hoursArray[1] / 60;
	echo round($hourCount, 2) . '<br>';

	$goldRecord = getRecords('SpiritBoar', 'gold_record');
	echo 'gold record: ' + $goldRecord;

	// Calculate chances.
	echo '<br>Attack test <br>';
	$gold = 		10000;
	$targetGold = 	1000;
    $baseChance = lcg_value() * 0.1 - 0.05;    
    //$baseChance = 0;    
    $baseRelative = 1 - $baseChance;
    $ratio = $gold / ($gold + $targetGold);

    $actualSuccess = min(0.75, $ratio);
    $actualSuccess = max(0.25, $actualSuccess);
    $actualSuccess += $baseChance;
    //$relativeFail = 1 - $relativeSuccess;
    
    
    
    //$actualFail = $baseFail + $baseRelative * $relativeFail;
    echo 'ratio: ' . $ratio . '<br>';
    echo 'Actual Success: ' . $actualSuccess . '<br>';
    //echo 'Actual Fail: ' . $actualFail . '<br>';
?>