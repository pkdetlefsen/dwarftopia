function getBoard(board) {
	getRequest(
		'./php/dashboard/' + board + '.php',
		showBoard,
		errorBoard,
		board
	)
}

function showBoard(responseText, board) {
	var boardElem = document.getElementById(board);		
	boardElem.innerHTML = responseText;	
}

function errorBoard(responseText, board) {
	var boardElem = document.getElementById(board);
	boardElem.innerHTML = 'There was an error fetching the ' + board + "!";
}

function getRequest(url, success, error, board) {
	var req = new XMLHttpRequest();
	if (typeof success != 'function') success = function () {};
	if (typeof error != 'function') error = function () {};
	req.onreadystatechange = function(){
		if(req.readyState == 4) {
			return req.status === 200 ? 
				success(req.responseText, board) : error(req.status, board);
		}
	}
	req.open("GET", url, true);
	req.send(null);
	return req;	
}

function lerp(a,b,u) {
    return (1-u) * a + u * b;
};

var el = document.getElementById('body');
var property = 'background-color';       
//var dark_red = {r:168, g:  56, b:  56};  
//var dark_green   = {r:  92, g: 145, b:39};
var dark_red = {r:200, g:  30, b:  30};  
var dark_green   = {r:  0, g: 110, b: 46};

var startColor = dark_red;
var endColor = dark_green;

function fade(element, property, start, end, duration) {
  var interval = duration / 500;
  var steps = duration/interval;
  var step_u = 1.0/steps;
  var u = 0.0;
  var theInterval = setInterval(function() {

    if (u >= 1.0) { 
    	clearInterval(theInterval);
    	fade(element, property, end, start, duration);
    }

    var r = parseInt(lerp(start.r, end.r, u));
    var g = parseInt(lerp(start.g, end.g, u));
    var b = parseInt(lerp(start.b, end.b, u));
    var colorname = 'rgb('+r+','+g+','+b+')';

    element.style.setProperty(property, colorname);
    u += step_u;

  }, interval);
};

/*
getBoard('leaderboard');	
getBoard('hall_of_fame');
getBoard('events');
*/

setInterval(function() {
	getBoard('leaderboard');	
	getBoard('hall_of_fame');
	getBoard('events');
}, 1000);

el.style.setProperty(property, 'rgb('+startColor.r+','+startColor.g+','+startColor.b+')');
fade(el,'background-color',startColor,endColor, 5 * 60 * 1000);